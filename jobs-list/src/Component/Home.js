import React, { useEffect, useState } from "react";
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import InputBase from '@material-ui/core/InputBase';
import '../Component/Home.css';
import { Pagination } from "@material-ui/lab";
import usePagination from "./Pagination";
import {Link} from 'react-router-dom';
import Divider from '@mui/material/Divider';
import axios from "axios";




function Home(){
    const [searchInput, setSearchInput] = useState('');
    const [filteredResults, setFilteredResults] = useState([]);
    const [response, setResponse] = useState([]);
    let [page, setPage] = useState(1);
    const PER_PAGE = 12;
    const count = Math.ceil(response.length / PER_PAGE);

    const _DATA = usePagination(response, PER_PAGE);

    const handleChange = (e, p) => {
        setPage(p);
        _DATA.jump(p);
    };

    const card = (
        <div className="row">
            {searchInput.length > 1 ? (
                filteredResults.map((item , index) => {
                    return (
                        <div  key={index} className="col-md-2">
                            <div className="card">
                                <Typography>
                                    {item.title? item.title: 'N/A'}
                                </Typography>
                                <Typography>
                                    {item.location.city? item.location.city : 'N/A'}
                                </Typography>
                                <Divider />
                                <Typography >
                                    {item.career_level? item.career_level : 'N/A'}
                                </Typography>
                                <Divider />
                                <Typography>
                                    {item.skills.slice(0, 2) ? item.skills.slice(0, 2).join(' , ') : 'N/A'}
                                </Typography>
                                <Typography>
                                    <Link to={{
                                        pathname: `/jobDetails`,
                                        state: item
                                    }}
                                    >
                                        <button className="btn btn-light button-view">view</button>
                                    </Link>
                                </Typography>
                            </div>
                        </div>
                    )
                })
            ) : (
                _DATA.currentData().map((item, index) => {
                    return (
                        <div key={index} className="col-md-2">
                            <div className="card">
                                <Typography>
                                    {item.title? item.title : 'N/A'}
                                </Typography>
                                <Typography >
                                    {item.location.city? item.location.city : 'N/A'}
                                </Typography>
                                <Divider variant="middle" />
                                <Typography >
                                    {item.career_level? item.career_level : 'N/A'}
                                </Typography>

                                <Divider variant="middle" />

                                <Typography>
                                    {item.skills.slice(0, 2) ? item.skills.slice(0, 2).join(' , ') : 'N/A'}
                                </Typography>
                                <Typography>
                                    <Link to={{
                                        pathname: `/jobDetails`,
                                        state: item
                                    }}
                                    >
                                        <button className="btn btn-light button-view">view</button>
                                    </Link>
                                </Typography>
                            </div>
                        </div>
                    )
                })
            )}


        </div>
    )

    const searchItems = (searchValue) => {
        setSearchInput(searchValue)
        if (searchInput !== '') {
            const filteredData = response.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setFilteredResults(filteredData)
        }
        else{
            setFilteredResults(response)
        }
    }
    const filteredData = response.filter((item) => {
        return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
    })
    useEffect(() => {
        var axios = require('axios');
        var FormData = require('form-data');
        var data = new FormData();


        var config = {
            method: 'get',
            url: 'https://devapi-indexer.elevatustesting.xyz/api/v1/jobs',
            headers: {
                'accept-company': '900a776e-a060-422e-a5e3-979ef669f16b',
                // ...data.getHeaders()
            },
            data : data
        };

        axios(config)
            .then(function (response) {

                response=response.data.results.jobs;
                setResponse(response)

            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);



    return (
        <div className="container">
            <div className="row">
                <Paper
                    component="form"
                    sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400 }}
                >
                    <div className="col-md-6">
                        <InputBase
                            sx={{ ml: 1, flex: 1 }}
                            placeholder="Search jobs"
                            inputProps={{ 'aria-label': 'search jobs' }}
                            onChange={(e) => searchItems(e.target.value)}
                        />
                    </div>
                    <div className="col-md-6 search-ic">
                        <button className="btn btn-primary">Search</button>
                    </div>
                </Paper>
            </div>
            {card}
            <Pagination className="pagination-style"
                        count={count}
                        size="large"
                        page={page}
                        variant="outlined"
                        shape="circle"
                        onChange={handleChange}

            />
        </div>
    )


}

export default Home;
