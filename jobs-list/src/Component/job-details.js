import React, {useState} from "react";
import card from "bootstrap-4-react/lib/components/card";
import './job-details.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faFacebook,
    faTwitter,
    faLinkedin
} from "@fortawesome/free-brands-svg-icons";
import Divider from "@mui/material/Divider";
import moment from 'moment'
import Typography from "@material-ui/core/Typography";

function JobDetails(props){
    const page_details = props.location.state;
    return(
            <div className="row">
                <div className="col-md-3">
                    <div className="card">
                        <Typography>
                            {page_details.title ? page_details.title : 'N/A'}
                        </Typography>
                        <Typography>
                            {page_details.location.city ? page_details.location.city : 'N/A'}
                        </Typography>
                        <Divider/>
                        <Typography>
                            {page_details.career_level ? page_details.career_level : 'N/A'}
                        </Typography>
                        <Divider/>
                        <Typography className="height-70">
                            {page_details.skills.slice(0, 2) ? page_details.skills.slice(0, 2).join(' , ') : 'N/A'}
                        </Typography>
                    </div>
                </div>


                <div className="col-md-9">
                    <div className="main-content p-3">
                        <div className="card-content">
                            <h1 className="card-header">
                                {page_details.title}<span className="btn btn-light button-list">{page_details.job_type}</span>
                            </h1>

                          <div className="date-content">
                              posted on:  {moment(page_details.posted_at).format('dddd MMM Mo YYYY')}
                          </div>
                            <p>
                                <header className="description-label">Description</header>
                                <p dangerouslySetInnerHTML={{__html: page_details.description}}></p>
                            </p>
                            <p>
                                <header  className="title-name">Requirements</header>
                                <p dangerouslySetInnerHTML={{__html: page_details.requirements}}></p>
                            </p>
                            <header className="title-name">Summary</header>
                            <div className="summary-list">
                                <div className={card}>
                                    <p>
                                        <span className="title-name">salary range: </span>{(page_details.salary.min + page_details.salary.max)/2}
                                    </p>
                                    <p>
                                        <span className="title-name">Industry:</span> {page_details.industry}
                                    </p>
                                    <p>
                                        <span className="title-name">Experience Required:</span> {page_details.years_of_experience}
                                    </p>
                                </div>
                                <div>
                                    <p>
                                        <span className="title-name">Major:</span> {page_details.major}
                                    </p>
                                    <p>
                                        <span className="title-name">Career Level:</span> {page_details.career_level}
                                    </p>
                                    <p>
                                        <span className="title-name">Minimum GPA: {page_details.gpa}</span>
                                    </p>
                                </div>
                            </div>


                            <div className="skills-list">
                                <div>
                                    <div  className="title-name"> required skills</div>
                                    {page_details.skills.map(skill =>{
                                        return <button className="btn btn-light button-list">{skill}</button>
                                    })}
                                </div>
                            </div>
                            <Divider/>

                            <div className="lang-content">
                                <p>Languages
                                    <span className="btn btn-light button-list">  {page_details.languages.map((lang,index) => {
                                        return lang['ar']
                                    })}</span>
                                    </p>
                            </div>
                            <Divider/>
                  <div className="footer-content">
                     <div className="social-media-icon">
                         <p className="social-media-share">Share</p>
                        <a href="https://www.facebook.com/Elevatus.io/"
                            >
                            <FontAwesomeIcon className="facebook-social" icon={faFacebook} size="3x"  />
                        </a>
                        <a href="https://twitter.com/elevatus_io?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" className="twitter social">
                            <FontAwesomeIcon className="twitter-social" icon={faTwitter} size="3x" />
                        </a>
                        <a href="https://www.linkedin.com/company/elevatusio'"
                           className="LinkedIn social">
                            <FontAwesomeIcon icon={faLinkedin} size="3x" />
                        </a>
                    </div>
                           <button className="btn btn-primary submit-button">Apply</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

        )
}

export default JobDetails
